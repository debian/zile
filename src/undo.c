/* undo.c generated by valac 0.52.3, the Vala compiler
 * generated from undo.vala, do not modify */

/* Undo facility functions

   Copyright (c) 1997-2020 Free Software Foundation, Inc.

   This file is part of GNU Zile.

   GNU Zile is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   GNU Zile is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, see <https://www.gnu.org/licenses/>.  */
/*
 * Undo action
 */

#include <glib-object.h>
#include <glib.h>
#include <estr.h>
#include <gobject/gvaluecollector.h>
#include <stdlib.h>
#include <string.h>
#include <gee.h>

typedef enum  {
	UNDO_TYPE_START_SEQUENCE,
	UNDO_TYPE_END_SEQUENCE,
	UNDO_TYPE_SAVE_BLOCK
} UndoType;

#define TYPE_UNDO_TYPE (undo_type_get_type ())

#define TYPE_UNDO (undo_get_type ())
#define UNDO(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_UNDO, Undo))
#define UNDO_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_UNDO, UndoClass))
#define IS_UNDO(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_UNDO))
#define IS_UNDO_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_UNDO))
#define UNDO_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_UNDO, UndoClass))

typedef struct _Undo Undo;
typedef struct _UndoClass UndoClass;
typedef struct _UndoPrivate UndoPrivate;
#define _immutable_estr_unref0(var) ((var == NULL) ? NULL : (var = (immutable_estr_unref (var), NULL)))
typedef struct _ParamSpecUndo ParamSpecUndo;

#define TYPE_BUFFER (buffer_get_type ())
#define BUFFER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_BUFFER, Buffer))
#define BUFFER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_BUFFER, BufferClass))
#define IS_BUFFER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_BUFFER))
#define IS_BUFFER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_BUFFER))
#define BUFFER_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_BUFFER, BufferClass))

typedef struct _Buffer Buffer;
typedef struct _BufferClass BufferClass;
typedef struct _BufferPrivate BufferPrivate;

#define TYPE_MARKER (marker_get_type ())
#define MARKER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_MARKER, Marker))
#define MARKER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_MARKER, MarkerClass))
#define IS_MARKER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_MARKER))
#define IS_MARKER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_MARKER))
#define MARKER_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_MARKER, MarkerClass))

typedef struct _Marker Marker;
typedef struct _MarkerClass MarkerClass;

#define TYPE_VAR_ENTRY (var_entry_get_type ())
#define VAR_ENTRY(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_VAR_ENTRY, VarEntry))
#define VAR_ENTRY_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_VAR_ENTRY, VarEntryClass))
#define IS_VAR_ENTRY(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_VAR_ENTRY))
#define IS_VAR_ENTRY_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_VAR_ENTRY))
#define VAR_ENTRY_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_VAR_ENTRY, VarEntryClass))

typedef struct _VarEntry VarEntry;
typedef struct _VarEntryClass VarEntryClass;

#define TYPE_REGION (region_get_type ())
#define REGION(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_REGION, Region))
#define REGION_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_REGION, RegionClass))
#define IS_REGION(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_REGION))
#define IS_REGION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_REGION))
#define REGION_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_REGION, RegionClass))

typedef struct _Region Region;
typedef struct _RegionClass RegionClass;
#define _region_unref0(var) ((var == NULL) ? NULL : (var = (region_unref (var), NULL)))
#define _undo_unref0(var) ((var == NULL) ? NULL : (var = (undo_unref (var), NULL)))

#define TYPE_LISP_FUNC (lisp_func_get_type ())
#define LISP_FUNC(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_LISP_FUNC, LispFunc))
#define LISP_FUNC_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_LISP_FUNC, LispFuncClass))
#define IS_LISP_FUNC(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_LISP_FUNC))
#define IS_LISP_FUNC_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_LISP_FUNC))
#define LISP_FUNC_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_LISP_FUNC, LispFuncClass))

typedef struct _LispFunc LispFunc;
typedef struct _LispFuncClass LispFuncClass;
#define _lisp_func_unref0(var) ((var == NULL) ? NULL : (var = (lisp_func_unref (var), NULL)))
typedef gboolean (*Function) (glong uniarg, GeeQueue* args);

struct _Undo {
	GTypeInstance parent_instance;
	volatile int ref_count;
	UndoPrivate * priv;
	UndoType type;
	gsize o;
	gboolean unchanged;
	ImmutableEstr* text;
	gsize size;
};

struct _UndoClass {
	GTypeClass parent_class;
	void (*finalize) (Undo *self);
};

struct _ParamSpecUndo {
	GParamSpec parent_instance;
};

struct _Buffer {
	GTypeInstance parent_instance;
	volatile int ref_count;
	BufferPrivate * priv;
	gchar* name;
	gchar* filename;
	Buffer* next;
	gsize goalc;
	Marker* mark;
	Marker* markers;
	GList* last_undop;
	GList* next_undop;
	GHashTable* vars;
	gboolean modified;
	gboolean nosave;
	gboolean needname;
	gboolean temporary;
	gboolean readonly;
	gboolean backup;
	gboolean noundo;
	gboolean autofill;
	gboolean isearch;
	gboolean mark_active;
	gchar* dir;
};

struct _BufferClass {
	GTypeClass parent_class;
	void (*finalize) (Buffer *self);
	gsize (*get_length) (Buffer* self);
	const gchar* (*get_eol) (Buffer* self);
};

static gpointer undo_parent_class = NULL;
extern Buffer* cur_bp;

GType undo_type_get_type (void) G_GNUC_CONST ;
gpointer undo_ref (gpointer instance);
void undo_unref (gpointer instance);
GParamSpec* param_spec_undo (const gchar* name,
                             const gchar* nick,
                             const gchar* blurb,
                             GType object_type,
                             GParamFlags flags);
void value_set_undo (GValue* value,
                     gpointer v_object);
void value_take_undo (GValue* value,
                      gpointer v_object);
gpointer value_get_undo (const GValue* value);
GType undo_get_type (void) G_GNUC_CONST ;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (Undo, undo_unref)
Undo* undo_new (void);
Undo* undo_construct (GType object_type);
static void undo_finalize (Undo * obj);
static GType undo_get_type_once (void);
void undo_save (UndoType type,
                gsize o,
                gsize osize,
                gsize size);
gpointer buffer_ref (gpointer instance);
void buffer_unref (gpointer instance);
GParamSpec* param_spec_buffer (const gchar* name,
                               const gchar* nick,
                               const gchar* blurb,
                               GType object_type,
                               GParamFlags flags);
void value_set_buffer (GValue* value,
                       gpointer v_object);
void value_take_buffer (GValue* value,
                        gpointer v_object);
gpointer value_get_buffer (const GValue* value);
GType buffer_get_type (void) G_GNUC_CONST ;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (Buffer, buffer_unref)
gpointer marker_ref (gpointer instance);
void marker_unref (gpointer instance);
GParamSpec* param_spec_marker (const gchar* name,
                               const gchar* nick,
                               const gchar* blurb,
                               GType object_type,
                               GParamFlags flags);
void value_set_marker (GValue* value,
                       gpointer v_object);
void value_take_marker (GValue* value,
                        gpointer v_object);
gpointer value_get_marker (const GValue* value);
GType marker_get_type (void) G_GNUC_CONST ;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (Marker, marker_unref)
gpointer var_entry_ref (gpointer instance);
void var_entry_unref (gpointer instance);
GParamSpec* param_spec_var_entry (const gchar* name,
                                  const gchar* nick,
                                  const gchar* blurb,
                                  GType object_type,
                                  GParamFlags flags);
void value_set_var_entry (GValue* value,
                          gpointer v_object);
void value_take_var_entry (GValue* value,
                           gpointer v_object);
gpointer value_get_var_entry (const GValue* value);
GType var_entry_get_type (void) G_GNUC_CONST ;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (VarEntry, var_entry_unref)
gpointer region_ref (gpointer instance);
void region_unref (gpointer instance);
GParamSpec* param_spec_region (const gchar* name,
                               const gchar* nick,
                               const gchar* blurb,
                               GType object_type,
                               GParamFlags flags);
void value_set_region (GValue* value,
                       gpointer v_object);
void value_take_region (GValue* value,
                        gpointer v_object);
gpointer value_get_region (const GValue* value);
GType region_get_type (void) G_GNUC_CONST ;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (Region, region_unref)
ImmutableEstr* buffer_get_region (Buffer* self,
                                  Region* r);
Region* region_new (gsize o1,
                    gsize o2);
Region* region_construct (GType object_type,
                          gsize o1,
                          gsize o2);
void undo_start_sequence (void);
gsize buffer_get_pt (Buffer* self);
void undo_end_sequence (void);
gpointer lisp_func_ref (gpointer instance);
void lisp_func_unref (gpointer instance);
GParamSpec* param_spec_lisp_func (const gchar* name,
                                  const gchar* nick,
                                  const gchar* blurb,
                                  GType object_type,
                                  GParamFlags flags);
void value_set_lisp_func (GValue* value,
                          gpointer v_object);
void value_take_lisp_func (GValue* value,
                           gpointer v_object);
gpointer value_get_lisp_func (const GValue* value);
GType lisp_func_get_type (void) G_GNUC_CONST ;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (LispFunc, lisp_func_unref)
LispFunc* last_command (void);
LispFunc* lisp_func_find (const gchar* name);
void undo_save_block (gsize o,
                      gsize osize,
                      gsize size);
GList* revert_action (GList* l);
void buffer_goto_offset (Buffer* self,
                         gsize o);
gboolean buffer_replace_estr (Buffer* self,
                              gsize del,
                              ImmutableEstr* es);
void undo_set_unchanged (GList* l);
void undo_init (void);
static gboolean __lambda135_ (glong uniarg,
                       GeeQueue* args);
void minibuf_error (const gchar* fmt,
                    ...);
gboolean buffer_warn_if_readonly (Buffer* self);
void minibuf_write (const gchar* fmt,
                    ...);
static gboolean ___lambda135__function (glong uniarg,
                                 GeeQueue* args);
LispFunc* lisp_func_new (const gchar* name,
                         Function func,
                         gboolean interactive,
                         const gchar* doc);
LispFunc* lisp_func_construct (GType object_type,
                               const gchar* name,
                               Function func,
                               gboolean interactive,
                               const gchar* doc);

static GType
undo_type_get_type_once (void)
{
	static const GEnumValue values[] = {{UNDO_TYPE_START_SEQUENCE, "UNDO_TYPE_START_SEQUENCE", "start-sequence"}, {UNDO_TYPE_END_SEQUENCE, "UNDO_TYPE_END_SEQUENCE", "end-sequence"}, {UNDO_TYPE_SAVE_BLOCK, "UNDO_TYPE_SAVE_BLOCK", "save-block"}, {0, NULL, NULL}};
	GType undo_type_type_id;
	undo_type_type_id = g_enum_register_static ("UndoType", values);
	return undo_type_type_id;
}

GType
undo_type_get_type (void)
{
	static volatile gsize undo_type_type_id__volatile = 0;
	if (g_once_init_enter (&undo_type_type_id__volatile)) {
		GType undo_type_type_id;
		undo_type_type_id = undo_type_get_type_once ();
		g_once_init_leave (&undo_type_type_id__volatile, undo_type_type_id);
	}
	return undo_type_type_id__volatile;
}

Undo*
undo_construct (GType object_type)
{
	Undo* self = NULL;
#line 29 "src/undo.vala"
	self = (Undo*) g_type_create_instance (object_type);
#line 29 "src/undo.vala"
	return self;
#line 331 "undo.c"
}

Undo*
undo_new (void)
{
#line 29 "src/undo.vala"
	return undo_construct (TYPE_UNDO);
#line 339 "undo.c"
}

static void
value_undo_init (GValue* value)
{
#line 29 "src/undo.vala"
	value->data[0].v_pointer = NULL;
#line 347 "undo.c"
}

static void
value_undo_free_value (GValue* value)
{
#line 29 "src/undo.vala"
	if (value->data[0].v_pointer) {
#line 29 "src/undo.vala"
		undo_unref (value->data[0].v_pointer);
#line 357 "undo.c"
	}
}

static void
value_undo_copy_value (const GValue* src_value,
                       GValue* dest_value)
{
#line 29 "src/undo.vala"
	if (src_value->data[0].v_pointer) {
#line 29 "src/undo.vala"
		dest_value->data[0].v_pointer = undo_ref (src_value->data[0].v_pointer);
#line 369 "undo.c"
	} else {
#line 29 "src/undo.vala"
		dest_value->data[0].v_pointer = NULL;
#line 373 "undo.c"
	}
}

static gpointer
value_undo_peek_pointer (const GValue* value)
{
#line 29 "src/undo.vala"
	return value->data[0].v_pointer;
#line 382 "undo.c"
}

static gchar*
value_undo_collect_value (GValue* value,
                          guint n_collect_values,
                          GTypeCValue* collect_values,
                          guint collect_flags)
{
#line 29 "src/undo.vala"
	if (collect_values[0].v_pointer) {
#line 393 "undo.c"
		Undo * object;
		object = collect_values[0].v_pointer;
#line 29 "src/undo.vala"
		if (object->parent_instance.g_class == NULL) {
#line 29 "src/undo.vala"
			return g_strconcat ("invalid unclassed object pointer for value type `", G_VALUE_TYPE_NAME (value), "'", NULL);
#line 400 "undo.c"
		} else if (!g_value_type_compatible (G_TYPE_FROM_INSTANCE (object), G_VALUE_TYPE (value))) {
#line 29 "src/undo.vala"
			return g_strconcat ("invalid object type `", g_type_name (G_TYPE_FROM_INSTANCE (object)), "' for value type `", G_VALUE_TYPE_NAME (value), "'", NULL);
#line 404 "undo.c"
		}
#line 29 "src/undo.vala"
		value->data[0].v_pointer = undo_ref (object);
#line 408 "undo.c"
	} else {
#line 29 "src/undo.vala"
		value->data[0].v_pointer = NULL;
#line 412 "undo.c"
	}
#line 29 "src/undo.vala"
	return NULL;
#line 416 "undo.c"
}

static gchar*
value_undo_lcopy_value (const GValue* value,
                        guint n_collect_values,
                        GTypeCValue* collect_values,
                        guint collect_flags)
{
	Undo ** object_p;
	object_p = collect_values[0].v_pointer;
#line 29 "src/undo.vala"
	if (!object_p) {
#line 29 "src/undo.vala"
		return g_strdup_printf ("value location for `%s' passed as NULL", G_VALUE_TYPE_NAME (value));
#line 431 "undo.c"
	}
#line 29 "src/undo.vala"
	if (!value->data[0].v_pointer) {
#line 29 "src/undo.vala"
		*object_p = NULL;
#line 437 "undo.c"
	} else if (collect_flags & G_VALUE_NOCOPY_CONTENTS) {
#line 29 "src/undo.vala"
		*object_p = value->data[0].v_pointer;
#line 441 "undo.c"
	} else {
#line 29 "src/undo.vala"
		*object_p = undo_ref (value->data[0].v_pointer);
#line 445 "undo.c"
	}
#line 29 "src/undo.vala"
	return NULL;
#line 449 "undo.c"
}

GParamSpec*
param_spec_undo (const gchar* name,
                 const gchar* nick,
                 const gchar* blurb,
                 GType object_type,
                 GParamFlags flags)
{
	ParamSpecUndo* spec;
#line 29 "src/undo.vala"
	g_return_val_if_fail (g_type_is_a (object_type, TYPE_UNDO), NULL);
#line 29 "src/undo.vala"
	spec = g_param_spec_internal (G_TYPE_PARAM_OBJECT, name, nick, blurb, flags);
#line 29 "src/undo.vala"
	G_PARAM_SPEC (spec)->value_type = object_type;
#line 29 "src/undo.vala"
	return G_PARAM_SPEC (spec);
#line 468 "undo.c"
}

gpointer
value_get_undo (const GValue* value)
{
#line 29 "src/undo.vala"
	g_return_val_if_fail (G_TYPE_CHECK_VALUE_TYPE (value, TYPE_UNDO), NULL);
#line 29 "src/undo.vala"
	return value->data[0].v_pointer;
#line 478 "undo.c"
}

void
value_set_undo (GValue* value,
                gpointer v_object)
{
	Undo * old;
#line 29 "src/undo.vala"
	g_return_if_fail (G_TYPE_CHECK_VALUE_TYPE (value, TYPE_UNDO));
#line 29 "src/undo.vala"
	old = value->data[0].v_pointer;
#line 29 "src/undo.vala"
	if (v_object) {
#line 29 "src/undo.vala"
		g_return_if_fail (G_TYPE_CHECK_INSTANCE_TYPE (v_object, TYPE_UNDO));
#line 29 "src/undo.vala"
		g_return_if_fail (g_value_type_compatible (G_TYPE_FROM_INSTANCE (v_object), G_VALUE_TYPE (value)));
#line 29 "src/undo.vala"
		value->data[0].v_pointer = v_object;
#line 29 "src/undo.vala"
		undo_ref (value->data[0].v_pointer);
#line 500 "undo.c"
	} else {
#line 29 "src/undo.vala"
		value->data[0].v_pointer = NULL;
#line 504 "undo.c"
	}
#line 29 "src/undo.vala"
	if (old) {
#line 29 "src/undo.vala"
		undo_unref (old);
#line 510 "undo.c"
	}
}

void
value_take_undo (GValue* value,
                 gpointer v_object)
{
	Undo * old;
#line 29 "src/undo.vala"
	g_return_if_fail (G_TYPE_CHECK_VALUE_TYPE (value, TYPE_UNDO));
#line 29 "src/undo.vala"
	old = value->data[0].v_pointer;
#line 29 "src/undo.vala"
	if (v_object) {
#line 29 "src/undo.vala"
		g_return_if_fail (G_TYPE_CHECK_INSTANCE_TYPE (v_object, TYPE_UNDO));
#line 29 "src/undo.vala"
		g_return_if_fail (g_value_type_compatible (G_TYPE_FROM_INSTANCE (v_object), G_VALUE_TYPE (value)));
#line 29 "src/undo.vala"
		value->data[0].v_pointer = v_object;
#line 531 "undo.c"
	} else {
#line 29 "src/undo.vala"
		value->data[0].v_pointer = NULL;
#line 535 "undo.c"
	}
#line 29 "src/undo.vala"
	if (old) {
#line 29 "src/undo.vala"
		undo_unref (old);
#line 541 "undo.c"
	}
}

static void
undo_class_init (UndoClass * klass,
                 gpointer klass_data)
{
#line 29 "src/undo.vala"
	undo_parent_class = g_type_class_peek_parent (klass);
#line 29 "src/undo.vala"
	((UndoClass *) klass)->finalize = undo_finalize;
#line 553 "undo.c"
}

static void
undo_instance_init (Undo * self,
                    gpointer klass)
{
#line 29 "src/undo.vala"
	self->ref_count = 1;
#line 562 "undo.c"
}

static void
undo_finalize (Undo * obj)
{
	Undo * self;
#line 29 "src/undo.vala"
	self = G_TYPE_CHECK_INSTANCE_CAST (obj, TYPE_UNDO, Undo);
#line 29 "src/undo.vala"
	g_signal_handlers_destroy (self);
#line 34 "src/undo.vala"
	_immutable_estr_unref0 (self->text);
#line 575 "undo.c"
}

static GType
undo_get_type_once (void)
{
	static const GTypeValueTable g_define_type_value_table = { value_undo_init, value_undo_free_value, value_undo_copy_value, value_undo_peek_pointer, "p", value_undo_collect_value, "p", value_undo_lcopy_value };
	static const GTypeInfo g_define_type_info = { sizeof (UndoClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) undo_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (Undo), 0, (GInstanceInitFunc) undo_instance_init, &g_define_type_value_table };
	static const GTypeFundamentalInfo g_define_type_fundamental_info = { (G_TYPE_FLAG_CLASSED | G_TYPE_FLAG_INSTANTIATABLE | G_TYPE_FLAG_DERIVABLE | G_TYPE_FLAG_DEEP_DERIVABLE) };
	GType undo_type_id;
	undo_type_id = g_type_register_fundamental (g_type_fundamental_next (), "Undo", &g_define_type_info, &g_define_type_fundamental_info, 0);
	return undo_type_id;
}

GType
undo_get_type (void)
{
	static volatile gsize undo_type_id__volatile = 0;
	if (g_once_init_enter (&undo_type_id__volatile)) {
		GType undo_type_id;
		undo_type_id = undo_get_type_once ();
		g_once_init_leave (&undo_type_id__volatile, undo_type_id);
	}
	return undo_type_id__volatile;
}

gpointer
undo_ref (gpointer instance)
{
	Undo * self;
	self = instance;
#line 29 "src/undo.vala"
	g_atomic_int_inc (&self->ref_count);
#line 29 "src/undo.vala"
	return instance;
#line 610 "undo.c"
}

void
undo_unref (gpointer instance)
{
	Undo * self;
	self = instance;
#line 29 "src/undo.vala"
	if (g_atomic_int_dec_and_test (&self->ref_count)) {
#line 29 "src/undo.vala"
		UNDO_GET_CLASS (self)->finalize (self);
#line 29 "src/undo.vala"
		g_type_free_instance ((GTypeInstance *) self);
#line 624 "undo.c"
	}
}

static gpointer
_undo_ref0 (gpointer self)
{
#line 55 "src/undo.vala"
	return self ? undo_ref (self) : NULL;
#line 633 "undo.c"
}

void
undo_save (UndoType type,
           gsize o,
           gsize osize,
           gsize size)
{
	Buffer* _tmp0_;
	Undo* u = NULL;
	Undo* _tmp1_;
	Undo* _tmp2_;
	Undo* _tmp3_;
	Buffer* _tmp12_;
	Undo* _tmp13_;
	Undo* _tmp14_;
#line 42 "src/undo.vala"
	_tmp0_ = cur_bp;
#line 42 "src/undo.vala"
	if (_tmp0_->noundo) {
#line 43 "src/undo.vala"
		return;
#line 656 "undo.c"
	}
#line 45 "src/undo.vala"
	_tmp1_ = undo_new ();
#line 45 "src/undo.vala"
	u = _tmp1_;
#line 46 "src/undo.vala"
	_tmp2_ = u;
#line 46 "src/undo.vala"
	_tmp2_->type = type;
#line 47 "src/undo.vala"
	_tmp3_ = u;
#line 47 "src/undo.vala"
	_tmp3_->o = o;
#line 49 "src/undo.vala"
	if (type == UNDO_TYPE_SAVE_BLOCK) {
#line 672 "undo.c"
		Undo* _tmp4_;
		Undo* _tmp5_;
		Buffer* _tmp6_;
		Region* _tmp7_;
		Region* _tmp8_;
		ImmutableEstr* _tmp9_;
		Undo* _tmp10_;
		Buffer* _tmp11_;
#line 50 "src/undo.vala"
		_tmp4_ = u;
#line 50 "src/undo.vala"
		_tmp4_->size = size;
#line 51 "src/undo.vala"
		_tmp5_ = u;
#line 51 "src/undo.vala"
		_tmp6_ = cur_bp;
#line 51 "src/undo.vala"
		_tmp7_ = region_new (o, o + osize);
#line 51 "src/undo.vala"
		_tmp8_ = _tmp7_;
#line 51 "src/undo.vala"
		_tmp9_ = buffer_get_region (_tmp6_, _tmp8_);
#line 51 "src/undo.vala"
		_immutable_estr_unref0 (_tmp5_->text);
#line 51 "src/undo.vala"
		_tmp5_->text = _tmp9_;
#line 51 "src/undo.vala"
		_region_unref0 (_tmp8_);
#line 52 "src/undo.vala"
		_tmp10_ = u;
#line 52 "src/undo.vala"
		_tmp11_ = cur_bp;
#line 52 "src/undo.vala"
		_tmp10_->unchanged = !_tmp11_->modified;
#line 707 "undo.c"
	}
#line 55 "src/undo.vala"
	_tmp12_ = cur_bp;
#line 55 "src/undo.vala"
	_tmp13_ = u;
#line 55 "src/undo.vala"
	_tmp14_ = _undo_ref0 (_tmp13_);
#line 55 "src/undo.vala"
	_tmp12_->last_undop = g_list_prepend (_tmp12_->last_undop, _tmp14_);
#line 41 "src/undo.vala"
	_undo_unref0 (u);
#line 719 "undo.c"
}

void
undo_start_sequence (void)
{
	Buffer* _tmp0_;
#line 59 "src/undo.vala"
	_tmp0_ = cur_bp;
#line 59 "src/undo.vala"
	if (_tmp0_ != NULL) {
#line 730 "undo.c"
		Buffer* _tmp1_;
		gsize _tmp2_;
		gsize _tmp3_;
#line 60 "src/undo.vala"
		_tmp1_ = cur_bp;
#line 60 "src/undo.vala"
		_tmp2_ = buffer_get_pt (_tmp1_);
#line 60 "src/undo.vala"
		_tmp3_ = _tmp2_;
#line 60 "src/undo.vala"
		undo_save (UNDO_TYPE_START_SEQUENCE, _tmp3_, (gsize) 0, (gsize) 0);
#line 742 "undo.c"
	}
}

void
undo_end_sequence (void)
{
	Buffer* _tmp0_;
#line 64 "src/undo.vala"
	_tmp0_ = cur_bp;
#line 64 "src/undo.vala"
	if (_tmp0_ != NULL) {
#line 754 "undo.c"
		GList* l = NULL;
		Buffer* _tmp1_;
		GList* _tmp2_;
		GList* _tmp3_;
		LispFunc* _tmp10_;
		LispFunc* _tmp11_;
		LispFunc* _tmp12_;
		LispFunc* _tmp13_;
		gboolean _tmp14_;
#line 65 "src/undo.vala"
		_tmp1_ = cur_bp;
#line 65 "src/undo.vala"
		_tmp2_ = _tmp1_->last_undop;
#line 65 "src/undo.vala"
		l = _tmp2_;
#line 66 "src/undo.vala"
		_tmp3_ = l;
#line 66 "src/undo.vala"
		if (g_list_length (_tmp3_) > ((guint) 0)) {
#line 774 "undo.c"
			GList* _tmp4_;
			gconstpointer _tmp5_;
			UndoType _tmp6_;
#line 67 "src/undo.vala"
			_tmp4_ = l;
#line 67 "src/undo.vala"
			_tmp5_ = _tmp4_->data;
#line 67 "src/undo.vala"
			_tmp6_ = ((Undo*) ((Undo*) _tmp5_))->type;
#line 67 "src/undo.vala"
			if (_tmp6_ == UNDO_TYPE_START_SEQUENCE) {
#line 786 "undo.c"
				Buffer* _tmp7_;
				GList* _tmp8_;
				GList* _tmp9_;
#line 68 "src/undo.vala"
				_tmp7_ = cur_bp;
#line 68 "src/undo.vala"
				_tmp8_ = l;
#line 68 "src/undo.vala"
				_tmp9_ = _tmp8_->next;
#line 68 "src/undo.vala"
				_tmp7_->last_undop = _tmp9_;
#line 798 "undo.c"
			} else {
#line 70 "src/undo.vala"
				undo_save (UNDO_TYPE_END_SEQUENCE, (gsize) 0, (gsize) 0, (gsize) 0);
#line 802 "undo.c"
			}
		}
#line 74 "src/undo.vala"
		_tmp10_ = last_command ();
#line 74 "src/undo.vala"
		_tmp11_ = _tmp10_;
#line 74 "src/undo.vala"
		_tmp12_ = lisp_func_find ("undo");
#line 74 "src/undo.vala"
		_tmp13_ = _tmp12_;
#line 74 "src/undo.vala"
		_tmp14_ = _tmp11_ != _tmp13_;
#line 74 "src/undo.vala"
		_lisp_func_unref0 (_tmp13_);
#line 74 "src/undo.vala"
		_lisp_func_unref0 (_tmp11_);
#line 74 "src/undo.vala"
		if (_tmp14_) {
#line 821 "undo.c"
			Buffer* _tmp15_;
			Buffer* _tmp16_;
			GList* _tmp17_;
#line 75 "src/undo.vala"
			_tmp15_ = cur_bp;
#line 75 "src/undo.vala"
			_tmp16_ = cur_bp;
#line 75 "src/undo.vala"
			_tmp17_ = _tmp16_->last_undop;
#line 75 "src/undo.vala"
			_tmp15_->next_undop = _tmp17_;
#line 833 "undo.c"
		}
	}
}

void
undo_save_block (gsize o,
                 gsize osize,
                 gsize size)
{
#line 80 "src/undo.vala"
	undo_save (UNDO_TYPE_SAVE_BLOCK, o, osize, size);
#line 845 "undo.c"
}

GList*
revert_action (GList* l)
{
	gconstpointer _tmp0_;
	UndoType _tmp1_;
	gconstpointer _tmp7_;
	UndoType _tmp8_;
	gconstpointer _tmp11_;
	UndoType _tmp12_;
	gconstpointer _tmp17_;
	GList* _tmp19_;
	GList* result = NULL;
#line 87 "src/undo.vala"
	_tmp0_ = l->data;
#line 87 "src/undo.vala"
	_tmp1_ = ((Undo*) ((Undo*) _tmp0_))->type;
#line 87 "src/undo.vala"
	if (_tmp1_ == UNDO_TYPE_END_SEQUENCE) {
#line 866 "undo.c"
		{
			GList* _tmp2_;
			gboolean _tmp3_ = FALSE;
#line 88 "src/undo.vala"
			_tmp2_ = l->next;
#line 88 "src/undo.vala"
			l = _tmp2_;
#line 88 "src/undo.vala"
			_tmp3_ = TRUE;
#line 88 "src/undo.vala"
			while (TRUE) {
#line 878 "undo.c"
				gconstpointer _tmp5_;
				UndoType _tmp6_;
#line 88 "src/undo.vala"
				if (!_tmp3_) {
#line 883 "undo.c"
					GList* _tmp4_;
#line 88 "src/undo.vala"
					_tmp4_ = revert_action (l);
#line 88 "src/undo.vala"
					l = _tmp4_;
#line 889 "undo.c"
				}
#line 88 "src/undo.vala"
				_tmp3_ = FALSE;
#line 88 "src/undo.vala"
				_tmp5_ = l->data;
#line 88 "src/undo.vala"
				_tmp6_ = ((Undo*) ((Undo*) _tmp5_))->type;
#line 88 "src/undo.vala"
				if (!(_tmp6_ != UNDO_TYPE_START_SEQUENCE)) {
#line 88 "src/undo.vala"
					break;
#line 901 "undo.c"
				}
			}
		}
	}
#line 91 "src/undo.vala"
	_tmp7_ = l->data;
#line 91 "src/undo.vala"
	_tmp8_ = ((Undo*) ((Undo*) _tmp7_))->type;
#line 91 "src/undo.vala"
	if (_tmp8_ != UNDO_TYPE_END_SEQUENCE) {
#line 912 "undo.c"
		Buffer* _tmp9_;
		gconstpointer _tmp10_;
#line 92 "src/undo.vala"
		_tmp9_ = cur_bp;
#line 92 "src/undo.vala"
		_tmp10_ = l->data;
#line 92 "src/undo.vala"
		buffer_goto_offset (_tmp9_, ((Undo*) ((Undo*) _tmp10_))->o);
#line 921 "undo.c"
	}
#line 93 "src/undo.vala"
	_tmp11_ = l->data;
#line 93 "src/undo.vala"
	_tmp12_ = ((Undo*) ((Undo*) _tmp11_))->type;
#line 93 "src/undo.vala"
	if (_tmp12_ == UNDO_TYPE_SAVE_BLOCK) {
#line 929 "undo.c"
		Buffer* _tmp13_;
		gconstpointer _tmp14_;
		gconstpointer _tmp15_;
		ImmutableEstr* _tmp16_;
#line 94 "src/undo.vala"
		_tmp13_ = cur_bp;
#line 94 "src/undo.vala"
		_tmp14_ = l->data;
#line 94 "src/undo.vala"
		_tmp15_ = l->data;
#line 94 "src/undo.vala"
		_tmp16_ = ((Undo*) ((Undo*) _tmp15_))->text;
#line 94 "src/undo.vala"
		buffer_replace_estr (_tmp13_, ((Undo*) ((Undo*) _tmp14_))->size, _tmp16_);
#line 944 "undo.c"
	}
#line 95 "src/undo.vala"
	_tmp17_ = l->data;
#line 95 "src/undo.vala"
	if (((Undo*) ((Undo*) _tmp17_))->unchanged) {
#line 950 "undo.c"
		Buffer* _tmp18_;
#line 96 "src/undo.vala"
		_tmp18_ = cur_bp;
#line 96 "src/undo.vala"
		_tmp18_->modified = FALSE;
#line 956 "undo.c"
	}
#line 98 "src/undo.vala"
	_tmp19_ = l->next;
#line 98 "src/undo.vala"
	result = _tmp19_;
#line 98 "src/undo.vala"
	return result;
#line 964 "undo.c"
}

void
undo_set_unchanged (GList* l)
{
	{
		GList* u_collection = NULL;
		GList* u_it = NULL;
#line 105 "src/undo.vala"
		u_collection = l;
#line 105 "src/undo.vala"
		for (u_it = u_collection; u_it != NULL; u_it = u_it->next) {
#line 977 "undo.c"
			Undo* _tmp0_;
			Undo* u = NULL;
#line 105 "src/undo.vala"
			_tmp0_ = _undo_ref0 ((Undo*) u_it->data);
#line 105 "src/undo.vala"
			u = _tmp0_;
#line 984 "undo.c"
			{
				Undo* _tmp1_;
#line 106 "src/undo.vala"
				_tmp1_ = u;
#line 106 "src/undo.vala"
				_tmp1_->unchanged = FALSE;
#line 105 "src/undo.vala"
				_undo_unref0 (u);
#line 993 "undo.c"
			}
		}
	}
}

static gboolean
__lambda135_ (glong uniarg,
              GeeQueue* args)
{
	Buffer* _tmp0_;
	Buffer* _tmp1_;
	Buffer* _tmp2_;
	GList* _tmp3_;
	Buffer* _tmp7_;
	Buffer* _tmp8_;
	GList* _tmp9_;
	GList* _tmp10_;
	gboolean result = FALSE;
#line 115 "src/undo.vala"
	_tmp0_ = cur_bp;
#line 115 "src/undo.vala"
	if (_tmp0_->noundo) {
#line 116 "src/undo.vala"
		minibuf_error ("Undo disabled in this buffer", NULL);
#line 117 "src/undo.vala"
		result = FALSE;
#line 117 "src/undo.vala"
		return result;
#line 1022 "undo.c"
	}
#line 120 "src/undo.vala"
	_tmp1_ = cur_bp;
#line 120 "src/undo.vala"
	if (buffer_warn_if_readonly (_tmp1_)) {
#line 121 "src/undo.vala"
		result = FALSE;
#line 121 "src/undo.vala"
		return result;
#line 1032 "undo.c"
	}
#line 123 "src/undo.vala"
	_tmp2_ = cur_bp;
#line 123 "src/undo.vala"
	_tmp3_ = _tmp2_->next_undop;
#line 123 "src/undo.vala"
	if (_tmp3_ == NULL) {
#line 1040 "undo.c"
		Buffer* _tmp4_;
		Buffer* _tmp5_;
		GList* _tmp6_;
#line 124 "src/undo.vala"
		minibuf_error ("No further undo information", NULL);
#line 125 "src/undo.vala"
		_tmp4_ = cur_bp;
#line 125 "src/undo.vala"
		_tmp5_ = cur_bp;
#line 125 "src/undo.vala"
		_tmp6_ = _tmp5_->last_undop;
#line 125 "src/undo.vala"
		_tmp4_->next_undop = _tmp6_;
#line 126 "src/undo.vala"
		result = FALSE;
#line 126 "src/undo.vala"
		return result;
#line 1058 "undo.c"
	}
#line 129 "src/undo.vala"
	_tmp7_ = cur_bp;
#line 129 "src/undo.vala"
	_tmp8_ = cur_bp;
#line 129 "src/undo.vala"
	_tmp9_ = _tmp8_->next_undop;
#line 129 "src/undo.vala"
	_tmp10_ = revert_action (_tmp9_);
#line 129 "src/undo.vala"
	_tmp7_->next_undop = _tmp10_;
#line 130 "src/undo.vala"
	minibuf_write ("Undo!", NULL);
#line 131 "src/undo.vala"
	result = TRUE;
#line 131 "src/undo.vala"
	return result;
#line 1076 "undo.c"
}

static gboolean
___lambda135__function (glong uniarg,
                        GeeQueue* args)
{
	gboolean result;
	result = __lambda135_ (uniarg, args);
#line 112 "src/undo.vala"
	return result;
#line 1087 "undo.c"
}

void
undo_init (void)
{
	LispFunc* _tmp0_;
	LispFunc* _tmp1_;
#line 112 "src/undo.vala"
	_tmp0_ = lisp_func_new ("undo", ___lambda135__function, TRUE, "Undo some previous changes.\n" \
"\t\tRepeat this command to undo more changes.");
#line 112 "src/undo.vala"
	_tmp1_ = _tmp0_;
#line 112 "src/undo.vala"
	_lisp_func_unref0 (_tmp1_);
#line 1101 "undo.c"
}

